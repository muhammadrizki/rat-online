<?php defined('BASEPATH') or exit('No direct script access allowed') ?>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase">Materi Rapat Anggota Tahunan</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tabbable-line">
            <ul class="nav nav-tabs ">
                <?php
                $i = 0;
                foreach ($data as $row) {
                    $classes = $i == 0 ? 'active' : '';
                    echo ' <li class="' . $classes . '">
                        <a href="#tab_' . $row->id . '" data-toggle="tab" aria-expanded="true">' . $row->judul . '</a>
                    </li>';
                    $i++;
                }
                ?>
            </ul>
            <div class="tab-content">
                <?php
                $i = 0;
                foreach ($data as $row) {
                    $classes = $i == 0 ? 'active' : '';

                    $konten = $row->konten;
                    $file = '';

                    if ($row->file != '') {
                        $file = ' <object data="' . base_url('/assets/apps/documents/' . $row->file . '#view=FitH,0&scrollbar=0&pagemode=thumbs') . '" type="application/pdf" width="100%" height="500px">
                                    <p>Your web browser doesn\'t have a PDF plugin.
                                        Instead you can <a href="' . base_url('/assets/apps/documents/' . $row->file) . '"> click here to
                                            download the PDF file.</a></p>
                                </object>';
                    }

                    echo '<div class="tab-pane ' . $classes . '" id="tab_' . $row->id . '">' .
                        $konten
                        .  $file . '
                        </div>';
                    $i++;
                }
                ?>


                <div id="adobe-dc-view" style="height: 360px; width: 500px;"></div>
                <script src="https://documentcloud.adobe.com/view-sdk/main.js"></script>
                <script type="text/javascript">
                    document.addEventListener("adobe_dc_view_sdk.ready", function() {
                        var adobeDCView = new AdobeDC.View({
                            clientId: "<YOUR_CLIENT_ID>",
                            divId: "adobe-dc-view"
                        });
                        adobeDCView.previewFile({
                            content: {
                                location: {
                                    url: "https://documentcloud.adobe.com/view-sdk-demo/PDFs/Bodea Brochure.pdf"
                                }
                            },
                            metaData: {
                                fileName: "Bodea Brochure.pdf"
                            }
                        }, {
                            embedMode: "SIZED_CONTAINER",
                            showDownloadPDF: false,
                            showPrintPDF: false,
                            showFullScreen: false
                        });
                    });
                </script>
            </div>
        </div>
    </div>
</div>