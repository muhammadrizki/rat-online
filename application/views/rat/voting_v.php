<?php defined('BASEPATH') or exit('No direct script access allowed') ?>
<script>
    function submitVote() {
        $("#dataProcessing").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('rat/submit_vote') ?>",
            dataType: "json",
            data: $("#votingForm").serialize(),
            success: function(data) {
                swal({
                        title: "Vote",
                        text: data.message,
                        type: data.status,
                        closeOnConfirm: true
                    },
                    function() {
                        location.reload();
                    });
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.error(xhr);
                sweetAlert("Vote", ajaxOptions + " - " + thrownError, "error");
            }
        });
    }
</script>
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase">voting</span>
        </div>
    </div>
    <div class="portlet-body form">
        <?php if ($entitled) { ?>
            <form class="form-horizontal" id="votingForm" role="form" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="col-md-3 control-label">Apakah anda setuju dengan materi yang disajikan?</label>
                        <div class="col-md-9">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="agreement" value="Y" checked> Setuju
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="agreement" value="N"> Tidak Setuju
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Tanggapan atas materi</label>
                    <div class="col-md-7">
                        <textarea name="feedback" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Kritik dan saran</label>
                    <div class="col-md-7">
                        <textarea name="suggestion" class="form-control" rows="3"></textarea>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="button" class="btn green" onclick="submitVote()">Submit</button>
                            <button type="button" class="btn default">Cancel</button>
                        </div>
                    </div>
                </div>
            </form>
        <?php } else { ?>
            <div>
                <h4 class="block bold" style="color: #15489a;">Terima kasih telah berpartisipasi</h4>
                <p> Terima kasih telah menggunakan hak suara Anda pada Rapat Anggota Tahunan Koperasi Simpan Pinjam Indosurya Cipta tahun buku 2019.</p>
            </div>
        <?php } ?>
    </div>
</div>