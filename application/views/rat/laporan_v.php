<div class="row">
    <div class="col-md-6">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-thunderbird">
                    <span class="caption-subject bold uppercase">Laporan Kehadiran</span>
                </div>
            </div>
            <div class="portlet-body">
                <h2>Jumlah Anggota: <?= $user_count ?></h2>
                <div style="margin:30px 0;">
                    <span class="font-dark bold uppercase"> anggota hadir: <?= $user_presence ?> </span><span>(<?= number_format(100 * $user_presence / $user_count, 2) ?>%)</span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $user_presence ?>" aria-valuemin="0" aria-valuemax="<?= $user_count ?>" style="width:<?= 100 * $user_presence / $user_count ?>%">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="portlet light bordered">
            <div class="portlet-title">
                <div class="caption font-red-thunderbird">
                    <span class="caption-subject bold uppercase">Laporan Hasil Voting</span>
                </div>
            </div>
            <div class="portlet-body">
                <h2>Jumlah Kehadiran: <?= $user_presence ?></h2>
                <?php
                $v_positive = $user_presence - $vote_negative;
                $vPercent1 = number_format(100 * $v_positive / $user_presence, 2) . '%';
                $vPercent2 = number_format(100 * $vote_negative / $user_presence, 2) . '%';
                ?>
                <div style="margin:30px 0;">
                    <span class="font-dark bold uppercase"> setuju: <?= $v_positive ?> </span><span>(<?= $vPercent1 ?>)</span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?= $v_positive ?>" aria-valuemin="0" aria-valuemax="<?= $user_presence ?>" style="width:<?= $vPercent1 ?>">
                        </div>
                    </div>

                    <span class="font-dark bold uppercase"> tidak setuju: <?= $vote_negative ?> </span><span>(<?= $vPercent2 ?>)</span>
                    <div class="progress">
                        <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="<?= $vote_negative ?>" aria-valuemin="0" aria-valuemax="<?= $user_presence ?>" style="width:<?= $vPercent2 ?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>