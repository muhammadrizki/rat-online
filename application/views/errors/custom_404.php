<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>

<link href="<?= base_url(); ?>/assets/pages/css/error.min.css" rel="stylesheet" id="style_components" type="text/css" />

<div class="row">
    <div class="col-md-12 page-404">
        <div class="number font-green"> 404 </div>
        <div class="details">
            <h3>Oops! You're lost.</h3>
            <p> We can not find the page you're looking for.
                <br>
                <a href="<?= site_url() ?>"> Return home </a></p>
        </div>
    </div>
</div>