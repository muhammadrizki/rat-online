<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <title>RAT Indosurya Simpan Pinjam</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN LAYOUT FIRST STYLES -->
    <link href="//fonts.googleapis.com/css?family=Oswald:400,300,700" rel="stylesheet" type="text/css" />
    <!-- END LAYOUT FIRST STYLES -->
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css">
    <link href="<?= base_url(); ?>/assets/global/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="<?= base_url(); ?>/assets/global/css/components-md.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="<?= base_url(); ?>/assets/global/css/plugins-md.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="<?= base_url(); ?>/assets/layouts/layout6/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url(); ?>/assets/layouts/layout6/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <!-- BEGIN CORE PLUGINS -->
    <script src="<?= base_url(); ?>/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="<?= base_url(); ?>/assets/global/plugins/sweetalert/sweetalert.min.js" type="text/javascript"></script>
    <!-- END CORE PLUGINS -->

    <link rel="shortcut icon" href="favicon.ico" />
</head>
<!-- END HEAD -->

<body class="page-md">
    <!-- BEGIN HEADER -->
    <header class="page-header">
        <nav class="navbar" role="navigation">
            <div class="container-fluid">
                <div class="havbar-header">
                    <!-- BEGIN LOGO -->
                    <a id="index" class="navbar-brand" href="<?= site_url() ?>">
                        <img src="<?= base_url(); ?>/assets/apps/img/logo.png" alt="Logo"> </a>
                    <!-- END LOGO -->
                </div>
            </div>
            <!--/container-->
        </nav>
    </header>
    <!-- END HEADER -->
    <!-- BEGIN CONTAINER -->
    <div class="container-fluid">
        <div class="page-content page-content-popup">
            <div class="page-content-fixed-header">
                <div class="content-header-menu">
                    <!-- BEGIN MENU TOGGLER -->
                    <button type="button" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="toggle-icon">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </span>
                    </button>
                    <!-- END MENU TOGGLER -->
                </div>
            </div>
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="icon-home font-red-thunderbird"></i>
                                <span class="title">Data Koperasi</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?= site_url('/company/profile') ?>" class="nav-link ">
                                        <span class="title">Profil</span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?= site_url('/company/pengurus') ?>" class="nav-link ">
                                        <span class="title">Pengurus</span>
                                    </a>
                                </li>
                                <li class="nav-item start ">
                                    <a href="<?= site_url('/company/anggota') ?>" class="nav-link ">
                                        <span class="title">Anggota</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a href="#" class="nav-link nav-toggle">
                                <i class="icon-briefcase font-red-thunderbird"></i>
                                <span class="title">Rapat Anggota</span>
                                <span class="arrow"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item">
                                    <a href="<?= site_url('/rat/materi') ?>" class="nav-link ">
                                        <span class="title">Materi</span>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="<?= site_url('/rat/voting') ?>" class="nav-link ">
                                        <span class="title">Voting</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <?php if ($this->session->userdata('is_admin') === 'Y') { ?>
                            <li class="nav-item">
                                <a href="<?= site_url('rat/laporan') ?>" class="nav-link ">
                                    <i class="icon-bar-chart font-red-thunderbird"></i>
                                    <span class="title">Laporan</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="<?= site_url('/mail') ?>" class="nav-link ">
                                    <i class="icon-envelope font-red-thunderbird"></i>
                                    <span class="title">Email</span>
                                </a>
                            </li>
                        <?php } ?>
                        <li class="nav-item">
                            <a href="<?= site_url('/auth/logout') ?>" class="nav-link ">
                                <i class="icon-logout font-red-thunderbird"></i>
                                <span class="title">Log out</span>
                            </a>
                        </li>
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <div class="page-fixed-main-content">
                <!-- BEGIN PAGE BASE CONTENT -->
                <?= $contents ?>
                <!-- END PAGE BASE CONTENT -->
            </div>
            <!-- BEGIN FOOTER -->
            <p class="copyright-v2"> <?= date('Y') ?> &copy; Koperasi Simpan Pinjam Indosurya Cipta </p>
            <a href="#index" class="go2top">
                <i class="icon-arrow-up"></i>
            </a>
            <!-- END FOOTER -->
        </div>
    </div>
    <!-- END CONTAINER -->
    <!-- BEGIN THEME GLOBAL SCRIPTS -->
    <script src="<?= base_url(); ?>/assets/global/scripts/app.min.js" type="text/javascript"></script>
    <!-- END THEME GLOBAL SCRIPTS -->
    <!-- BEGIN THEME LAYOUT SCRIPTS -->
    <script src="<?= base_url(); ?>/assets/layouts/layout6/scripts/layout.min.js" type="text/javascript"></script>
    <!-- END THEME LAYOUT SCRIPTS -->
</body>

</html>