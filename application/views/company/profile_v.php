<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase"> Profil Koperasi</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <img style="width: 100%" src="<?= base_url('/assets/apps/img/grha-surya.jpg') ?>" alt="">
            </div>
            <div class="col-md-6 col-sm-12">
                <p> Koperasi Simpan Pinjam Indosurya Cipta merupakan Koperasi Simpan Pinjam yang sudah mendapatkan ijin dari Kementrian Koperasi dan Usaha Kecil dan Menengah Republik Indonesia sejak tanggal 27 September 2012 dengan nomor badan hukum 430/BH/XII.1/-1.829.31/XI/2012 dan Nomor Induk Koperasi (NIK) 3173080020001 selanjutnya disebut “Indosurya Simpan Pinjam”. </p>
                <p> Indosurya Simpan Pinjam, berusaha untuk membantu para anggotanya dalam mengembangkan usahanya pada saat mereka memerlukan modal kerja. Indosurya Simpan Pinjam menghimpun dana dari anggota dan calon anggota yang kemudian menyalurkan kembali dana tersebut kepada anggota dan calon anggota dalam bentuk pinjaman. </p>
                <p> Saat ini pelaksanaan Indosurya Simpan Pinjam dilakukan oleh pengurus dan pengelola yang sangat berpengalaman dalam dunia keuangan, terutama perbankan. Selain itu keseluruhan manajemen Indosurya Simpan Pinjam sangat menjunjung tinggi integritas dalam mengembangkan usahanya dan dalam mengelola dana.</p>
            </div>
        </div>
    </div>
</div>