<?php defined('BASEPATH') or exit('No direct script access allowed') ?>
<script>
    $(document).ready(function() {
        submitPopulate();

    });

    function submitPopulate() {
        $("#dataProcessing").show();
        $.ajax({
            type: "POST",
            url: "<?= site_url('company/populate_anggota') ?>",
            dataType: "json",
            data: $("#search_filter").serialize(),
            success: function(data) {
                populateTable(data);
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.error(xhr);
                sweetAlert("Data Table", ajaxOptions + " - " + thrownError, "error");
            }
        });
    }

    function populateTable(data) {
        $("#dataProcessing").hide();
        $("#datatable_request").dataTable({
            "data": data,
            "destroy": true,
            "columns": [{
                    "title": "No",
                    "data": "NO",
                    "width": "10%"
                },
                {
                    "title": "Nama Anggota",
                    "data": "NAMA"
                },
            ],
            "lengthMenu": [
                [10, 20, 50, -1],
                [10, 20, 50, "All"]
            ],
            "filter": true,
            "ordering": false,
            "aaSorting": [],
            "pageLength": 10,
            "language": {
                "lengthMenu": "_MENU_ records",
            }
        }).show();
    }
</script>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase">Anggota Koperasi</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="row">
            <div class="col-md-12">
                <div id="dataProcessing" class="alert alert-mini alert-info text-center display-none">
                    Processing...
                </div>
                <table class="table table-striped table-bordered table-hover" id="datatable_request" style="font-size:10pt"></table>
            </div>
        </div>
    </div>
</div>