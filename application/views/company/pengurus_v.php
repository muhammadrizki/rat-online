<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase">Pengurus Koperasi</span>
        </div>
    </div>
    <div class="portlet-body">
        <div class="mt-element-card mt-card-round mt-element-overlay">
            <div class="row">
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="mt-card-item">
                        <div class="mt-card-avatar mt-overlay-1 mt-scroll-up">
                            <img src="<?= base_url() ?>/assets/pages/img/avatars/team7.jpg">
                        </div>
                        <div class="mt-card-content">
                            <h3 class="mt-card-name">Hugh Jackman</h3>
                            <p class="mt-card-desc font-grey-mint">Human Resource</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="mt-card-item">
                        <div class="mt-card-avatar mt-overlay-1">
                            <img src="<?= base_url() ?>/assets/pages/img/avatars/team5.jpg">
                        </div>
                        <div class="mt-card-content">
                            <h3 class="mt-card-name">Jennifer Lawrence</h3>
                            <p class="mt-card-desc font-grey-mint">Creative Director</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="mt-card-item">
                        <div class="mt-card-avatar mt-overlay-1 mt-scroll-down">
                            <img src="<?= base_url() ?>/assets/pages/img/avatars/team6.jpg">
                        </div>
                        <div class="mt-card-content">
                            <h3 class="mt-card-name">Kate Beck</h3>
                            <p class="mt-card-desc font-grey-mint">Executive Manager</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                    <div class="mt-card-item">
                        <div class="mt-card-avatar mt-overlay-1 mt-scroll-left">
                            <img src="<?= base_url() ?>/assets/pages/img/avatars/team8.jpg">
                        </div>
                        <div class="mt-card-content">
                            <h3 class="mt-card-name">Gwen Parker</h3>
                            <p class="mt-card-desc font-grey-mint">Finance Manager</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>