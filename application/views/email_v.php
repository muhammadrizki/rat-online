<script>
    $(document).ready(function() {

    });

    function sendInvitaion() {
        $.ajax({
            type: "POST",
            url: "<?= site_url('mail/send_invitation') ?>",
            dataType: "json",
            success: function(data) {
                // populateTable(data);
                location.reload();
            },
            error: function(xhr, ajaxOptions, thrownError) {
                console.error(xhr);
                sweetAlert("Send Invitation", ajaxOptions + " - " + thrownError, "error");
            }
        });
    }
</script>

<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption font-red-thunderbird">
            <span class="caption-subject bold uppercase">email invitation</span>
        </div>
    </div>
    <div class="portlet-body">
        <div style="margin: 30px">
            <?php
            $total = 0;

            foreach ($data as $row) {
                $total += $row->jumlah;
            }

            echo '<h2>TOTAL: ' . $total . '</h2>';

            echo '<div style="padding-top: 40px;">';

            foreach ($data as $row) {
                $classes = "progress-bar-success";
                if ($row->status === 'belum terkirim') {
                    $classes = "progress-bar-warning";
                }

                echo '<div class="font-dark bold uppercase">' .  $row->status . ': ' . $row->jumlah . '</div>';
                echo '<div class="progress">
                            <div class="progress-bar ' . $classes . '" role="progressbar" aria-valuenow="' . $row->jumlah . '" aria-valuemin="0" aria-valuemax="' . $total . '" style="width: ' . 100 * $row->jumlah / $total . '%">
                            </div>
                        </div>';
            }

            echo '</div>';

            ?>
        </div>
    </div>
</div>