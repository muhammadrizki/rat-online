<?php defined('BASEPATH') or exit('No direct script access allowed');

class User_m extends CI_Model
{

    private $table_name = "ids_user";
    private $primary_key = "id";

    public function __construct()
    {
        parent::__construct();
    }

    function login_user($username, $password)
    {

        $this->db->where(array(
            'username' => $username,
            'password' => md5($password),
            'is_aktif' => 'Y'
        ));
        $this->db->select("id, nama, is_admin");
        return $this->db->get($this->table_name);
    }

    function source($id)
    {
        $this->db->where((is_array($id) ? $id : array($this->primary_key => $id)));
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function source_num($where = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table_name);
        return $query->num_rows();
    }

    function all_source($where = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        $this->db->select("id, nama, username, email ");
        $this->db->order_by("$this->primary_key DESC");
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    function update_source($data, $id)
    {
        $this->db->update($this->table_name, $data, (is_array($id) ? $id : array($this->primary_key => $id)));
        return true;
    }

    function delete_source($id)
    {
        $this->db->delete($this->table_name, (is_array($id) ? $id : array($this->primary_key => $id)));
        return false;
    }
}
