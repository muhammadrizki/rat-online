<?php defined('BASEPATH') or exit('No direct script access allowed');

class Email_m extends CI_Model
{

    private $table_name;
    private $primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'ids_email';
        $this->primary_key = $this->table_name . '.id';
    }

    public function create_source($data)
    {
        $return = true;
        $this->db->insert($this->table_name, $data);
        $return = $this->db->insert_id();
        return  $return;
    }

    function all_source($where = NULL, $limit = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        if ($limit) {
            $this->db->limit($limit);
        }
        $this->db->order_by($this->primary_key, "ASC");
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    function update_source($data, $id)
    {
        $this->db->update($this->table_name, $data, (is_array($id) ? $id : array($this->primary_key => $id)));
        return true;
    }

    function delete_source($id)
    {
        $this->db->delete($this->table_name, (is_array($id) ? $id : array($this->primary_key => $id)));
        return false;
    }

    function get_status($subject)
    {

        $query = "SELECT COUNT(1) jumlah, 'belum terkirim' status FROM {$this->table_name} WHERE STATUS = 'N' AND subject = '" . $subject . "'
                    UNION
                SELECT COUNT(1) jumlah, 'terkirim' status FROM {$this->table_name} WHERE STATUS = 'S' AND subject = '" . $subject . "'";
        return $this->db->query($query)->result();
    }
}
