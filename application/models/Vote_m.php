<?php defined('BASEPATH') or exit('No direct script access allowed');

class Vote_m extends CI_Model
{

    private $table_name;
    private $primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'ids_vote';
        $this->primary_key = $this->table_name . '.id';
    }

    function source($id)
    {
        $this->db->where((is_array($id) ? $id : array($this->primary_key => $id)));
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function source_num($where = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        $query = $this->db->get($this->table_name);
        return $query->num_rows();
    }

    function all_source($where = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        $this->db->order_by("$this->primary_key ASC");
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    public function create_source($data)
    {
        $return = true;
        $this->db->insert($this->table_name, $data);
        $return = $this->db->insert_id();
        return  $return;
    }

    function update_source($data, $id)
    {
        $this->db->update($this->table_name, $data, (is_array($id) ? $id : array($this->primary_key => $id)));
        return true;
    }

    function delete_source($id)
    {
        $this->db->delete($this->table_name, (is_array($id) ? $id : array($this->primary_key => $id)));
        return false;
    }
}
