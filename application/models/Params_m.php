<?php defined('BASEPATH') or exit('No direct script access allowed');

class Params_m extends CI_Model
{

    private $table_name;
    private $primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'ids_params';
        $this->primary_key = $this->table_name . '.param_code';
    }

    function source($id)
    {
        $this->db->where((is_array($id) ? $id : array($this->primary_key => $id)));
        $query = $this->db->get($this->table_name);
        return $query->row()->param_value;
    }
}
