<?php defined('BASEPATH') or exit('No direct script access allowed');

class Materi_m extends CI_Model
{

    private $table_name;
    private $primary_key;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'ids_materi';
        $this->primary_key = $this->table_name . '.id';
        $this->sort_key = $this->table_name . '.no_urut';
    }

    function source($id)
    {
        $this->db->where((is_array($id) ? $id : array($this->primary_key => $id)));
        $query = $this->db->get($this->table_name);
        return $query->row();
    }

    function all_source($where = NULL)
    {
        if ($where) {
            $this->db->where($where);
        }
        // $this->db->select("id, nama, username, email ");
        $this->db->order_by("$this->sort_key ASC");
        $query = $this->db->get($this->table_name);
        return $query->result();
    }

    function update_source($data, $id)
    {
        $this->db->update($this->table_name, $data, (is_array($id) ? $id : array($this->primary_key => $id)));
        return true;
    }

    function delete_source($id)
    {
        $this->db->delete($this->table_name, (is_array($id) ? $id : array($this->primary_key => $id)));
        return false;
    }
}
