<?php defined('BASEPATH') or exit('No direct script access allowed');

class Login_log_m extends CI_Model
{

    private $table_name;

    public function __construct()
    {
        parent::__construct();
        $this->table_name = 'ids_login_log';
    }

    function create_source($data)
    {
        $return = true;
        $this->db->insert($this->table_name, $data);
        return $return;
    }

    function count_log($user_id, $start, $end)
    {
        $query = "SELECT * FROM {$this->table_name}
                    WHERE user_id = $user_id 
                        AND time BETWEEN '$start' AND '$end' 
                    ";
        $return = $this->db->query($query);
        return $return->num_rows();
    }

    function count_presence($start, $end)
    {
        $query = "SELECT COUNT(DISTINCT(user_id)) users FROM {$this->table_name}
                    WHERE time BETWEEN '$start' AND '$end' 
                    ";
        $return = $this->db->query($query);
        return $return->row()->users;
    }
}
