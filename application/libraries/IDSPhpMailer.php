<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

class IDSPhpMailer
{

    public $mail;

    function __construct()
    {
        $this->ci = &get_instance();
        $this->mail = new PHPMailer(true);
    }

    public function send_mail($subject, $bodyMessage, $toAddress, $replyTo = 'portal.information@indosuryaksp.com', $aliasName = '', $file = NULL)
    {

        $this->mail->IsSMTP(); // we are going to use SMTP
        $this->mail->SMTPDebug  = false;
        $this->mail->SMTPAuth   = true; // enabled SMTP authentication
        $this->mail->SMTPOptions = array(
            'ssl' => array(
                'verify_peer' => false,
                'verify_peer_name' => false,
                'allow_self_signed' => true
            )
        );

        $this->mail->IsHTML(true);

        if ($aliasName == "") {
            $aliasName = $this->ci->config->item('smtp_user_alias');
        }

        //$this->mail->Host       =   "103.11.75.20" ;//$this->ci->config->item('smtp_host') ; 
        $this->mail->Host       =  $this->ci->config->item('smtp_host');
        $this->mail->Port       =  $this->ci->config->item('smtp_port');
        $this->mail->Username   =  $this->ci->config->item('smtp_user');
        $this->mail->Password   =  $this->ci->config->item('smtp_pass');
        $this->mail->SetFrom($this->ci->config->item('smtp_user'), $this->ci->config->item('smtp_user_alias'));
        $this->mail->AddReplyTo($replyTo);
        $this->mail->Subject    =  $subject;
        $this->mail->Body       =  $bodyMessage;

        if ($file) {
            $this->mail->addAttachment($file);
        }

        $checkAddress = strpos($toAddress, ",");

        if ($checkAddress === false) {
            $this->mail->AddAddress($toAddress, "");
        } else {
            $explodeAddress = explode(",", $toAddress);
            for ($x = 0; $x < count($explodeAddress); $x++) {
                $this->mail->AddAddress($explodeAddress[$x], "");
            }
        }

        $data["recipient"] = $toAddress;
        if (!$this->mail->Send()) {
            $data["message"] = "Error: " . $this->mail->ErrorInfo;
            $data["status"] = "error";
        } else {
            $data["message"] = "Message sent correctly!";
            $data["status"] = "success";
        }

        return json_encode($data);
    }
}
