<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Auth extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('params_m');
        $this->load->model('login_log_m');
    }

    public function index()
    {
        $this->load->view('index_view');
    }

    public function login()
    {
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        $result = $this->user_m->login_user($username, $password);

        $login = false;
        $message = 'Invalid Login';

        $RATStartDate = $this->params_m->source('RAT_START_DATE');
        $RATEndDate = $this->params_m->source('RAT_END_DATE');

        if ($result->num_rows() > 0) {
            $data = $result->row();
            $params = array(
                'user_id' => $data->id,
                'user_nama' => $data->nama,
                'is_admin' => $data->is_admin,
                'isLoggedIn' => true
            );

            if ($params['is_admin'] !== 'Y') {
                $startDate = new DateTime($RATStartDate);
                $endDate = new DateTime($RATEndDate);
                $today = new DateTime(date("Y-m-d"));

                $loginLog = $this->login_log_m->count_log($data->id, $RATStartDate, $RATEndDate);

                if ($startDate > $today) {
                    $message = 'Rapat Anggota Tahunan Belum Dimulai';
                } else if ($today > $endDate) {
                    $message = 'Rapat Anggota Tahunan Telah Selesai';
                } else if ($loginLog > 0) {
                    $message = 'Anda telah menghadiri RAT';
                } else
                    $login = true;
            } else
                $login = true;
        }

        if ($login) {
            $this->login_log_m->create_source(array('user_id' => $data->id));
            $this->session->set_userdata($params);
            redirect();
        } else {
            $this->session->set_flashdata('error', $message);
            redirect('/auth');
        }
    }

    public function logout()
    {
        $this->session->unset_userdata('isLoggedIn');
        redirect('/auth');
    }
}
