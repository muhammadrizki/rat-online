<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('vote_m');
        $this->load->model('materi_m');
        $this->load->model('params_m');
        $this->load->model('login_log_m');
    }

    // MATERI
    public function materi()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $data = $this->get_materi();
            $this->template->load('template', 'rat/materi_v', array('data' => $data));
        } else {
            $this->load->view('index_view');
        }
    }

    public function get_materi()
    {
        if ($this->session->userdata('isLoggedIn')) {
            if ($data = $this->materi_m->all_source(array('is_aktif' => 'Y'))) {
                return $data;
            }
            return array();
        }
        return array();
    }

    // VOTING
    public function voting()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $countResponse = count($this->get_response());
            $entitled = $countResponse > 0 ? false : true;
            $this->template->load('template', 'rat/voting_v', array('entitled' => $entitled));
        } else {
            $this->load->view('index_view');
        }
    }

    public function get_response()
    {
        $userID = $this->session->userdata('user_id');
        return $this->vote_m->all_source(array('user_id' => $userID));
    }

    public function submit_vote()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $arr = array('agreement', 'feedback', 'suggestion');
            for ($i = 0; $i < count($arr); $i++) ${$arr[$i]} = $this->input->post($arr[$i], true);

            $return["status"] = "error";
            $return["message"] = "System error.";
            $userID = $this->session->userdata('user_id');

            $data = array(
                'user_id'       => $userID,
                'setuju'        => $agreement,
                'tanggapan'     => $feedback,
                'saran'         => $suggestion
            );

            $countResponse = count($this->get_response());

            if ($countResponse > 0) {
                $return["message"] = "Anda telah menggunakan hak suara sebelumnya";
            } else {
                if ($this->vote_m->create_source($data)) {
                    $return["status"] = "success";
                    $return["message"] = "Terima kasih telah melakukan voting";
                }
            }

            echo json_encode($return);
        }
    }

    public function laporan()
    {
        $RATStartDate = $this->params_m->source('RAT_START_DATE');
        $RATEndDate = $this->params_m->source('RAT_END_DATE');

        $data = array(
            'user_presence' => $this->login_log_m->count_presence($RATStartDate, $RATEndDate),
            'user_count'    => $this->user_m->source_num(array('is_aktif' => 'Y')),
            'vote_positive' => $this->vote_m->source_num(array('setuju' => 'Y')),
            'vote_negative' => $this->vote_m->source_num(array('setuju' => 'N'))
        );

        $this->template->load('template', 'rat/laporan_v', $data);
    }
}
