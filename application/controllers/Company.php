<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Company extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_m');
    }

    // PROFILE
    public function profile()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $this->template->load('template', 'company/profile_v');
        } else {
            $this->load->view('index_view');
        }
    }

    // ANGGOTA
    public function anggota()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $this->template->load('template', 'company/anggota_v');
        } else {
            $this->load->view('index_view');
        }
    }

    public function populate_anggota()
    {
        if ($this->session->userdata('isLoggedIn')) {

            if ($data = $this->user_m->all_source(array())) {
                $i = 0;
                $n = 1;
                foreach ($data as $row) {
                    $rows[$i]['NO'] = $n;
                    $rows[$i]['NAMA'] = $row->nama;

                    $action = '';
                    $rows[$i]['ACTION'] = '';

                    if ($action) {
                        $rows[$i]['ACTION'] = '<div style="width:50px;margin:auto;text-align:center">' . $action . '</div>';
                    }

                    $i++;
                    $n++;
                }
            } else $rows = array();

            echo json_encode($rows, true);
        } else echo '[]';
    }

    // PENGURUS
    public function pengurus()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $this->template->load('template', 'company/pengurus_v');
        } else {
            $this->load->view('index_view');
        }
    }
}
