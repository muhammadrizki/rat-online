<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends CI_Controller
{

    public function index()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $this->template->load('template', 'welcome');
        } else {
            $this->load->view('index_view');
        }
    }
}
