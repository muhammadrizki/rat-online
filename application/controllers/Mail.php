<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Mail extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('user_m');
        $this->load->model('email_m');
        $this->load->model('params_m');
    }

    public function index()
    {
        if ($this->session->userdata('isLoggedIn')) {
            if ($this->session->userdata('is_admin') === 'Y') {
                $data = $this->populate();
                $this->template->load('template', 'email_v', array('data' => $data));
            } else
                $this->template->load('template', 'errors/custom_404');
        } else
            $this->load->view('index_view');
    }

    public function populate()
    {
        if ($this->session->userdata('isLoggedIn')) {
            $data = $this->email_m->get_status('Undangan Rapat Anggota Tahunan');
            return $data;
        } else return '[]';
    }

    public function send_invitation($limit = 10)
    {
        $condition = array(
            'status'    => 'N'
        );

        $data = $this->email_m->all_source($condition, $limit);
        $mail = new IDSPhpMailer();
        $results = array();

        foreach ($data as $row) {
            $mailResult = $mail->send_mail($row->subject, $row->body, $row->email);

            $updateData = array(
                'status' => 'S',
                'response' => $mailResult
            );

            $results = array_merge($results, array(json_decode($mailResult)));

            $this->email_m->update_source($updateData, $row->id);
        }

        echo json_encode($results);
    }

    public function generate()
    {

        $users = $this->user_m->all_source(array('is_aktif' => 'N', 'password is null' => null));

        foreach ($users as $user) {
            if (filter_var($user->email, FILTER_VALIDATE_EMAIL)) {
                $password = get_random_password();
                $updateData = array('is_aktif' => 'Y', 'password' => md5($password));

                $this->user_m->update_source($updateData, $user->id);

                $RATStartDate = $this->params_m->source('RAT_START_DATE');
                $RATEndDate = $this->params_m->source('RAT_END_DATE');

                $startDate = new DateTime($RATStartDate);
                $endDate = new DateTime($RATEndDate);


                $html = '   Kepada Yth.<br/>
                            Bapak/Ibu ' . $user->nama . ' ,
                            <br/><br/>
                            <p>
                                Indonsurya Simpan Pinjam mengadakan Rapat Anggota Tahunan secara daring untuk tahun buku 2019 yang akan dilaksanakan mulai tanggal
                                ' . $startDate->format('d-m-Y') . ' sampai tanggal ' . $endDate->format('d-m-Y') . '
                            </p>
                            <p>Berikut adalah data login anda:<br/>
                            Website     : <a href="' . base_url() . '">' . base_url() . '</a><br/>
                            Username    : ' . $user->username . '<br/>
                            Password    : ' . $password . '</p>
                            <p>Anda dapat mengakses website tersebut pada tanggal di atas. </p>
                            <p>Kami harapkan partisipasi Bapak/Ibu anggota pada acara ini.</p>
                            <br/>
                            Terima Kasih
                            <br/><br/>
                            Indonsurya Simpan Pinjam';

                $data = array(
                    'email'     => $user->email,
                    'subject'   => 'Undangan Rapat Anggota Tahunan',
                    'body'      => $html,
                    'status'    => 'N',
                );

                $this->email_m->create_source($data);
            }
        }

        echo json_encode($users);
    }
}
